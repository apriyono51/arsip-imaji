import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUsersDto } from './dto/create-users.dto';
import { UpdateUsersDto } from './dto/update-users.dto';
import { Brackets, EntityNotFoundError, ILike, Repository } from 'typeorm';
import { Users } from './entities/users.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as uuid from 'uuid';
import { hashPassword } from '../helper/hash_password';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
  ) {}

  async create(createUsersDto: CreateUsersDto) {
    const isExistEmail = await this.findByEmail(createUsersDto.email);

    if (isExistEmail) {
      throw new HttpException(
        {
          statusCode: HttpStatus.BAD_REQUEST,
          message: 'email_is_exist',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const isExistUsername = await this.findByUsername(createUsersDto.username);

    if (isExistUsername) {
      throw new HttpException(
        {
          statusCode: HttpStatus.BAD_REQUEST,
          message: 'username_is_exist',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const salt = uuid.v4();

    await this.usersRepository.insert({
      ...createUsersDto,
      password: await hashPassword(createUsersDto.password, salt),
      salt: salt,
    });
  }

  findByUsername(username) {
    return this.usersRepository.findOne({
      where: {
        username: ILike(username),
      },
    });
  }

  findByEmail(email) {
    return this.usersRepository.findOne({
      where: {
        email: ILike(email),
      },
    });
  }

  findByUsernameOrEmail(usernameOrEmail) {
    return this.usersRepository
      .createQueryBuilder('users')
      .andWhere(
        new Brackets((query) => {
          query
            .where('LOWER(username) like LOWER(:username)', {
              username: `%${usernameOrEmail}%`,
            })
            .orWhere('LOWER(email) like LOWER(:email)', {
              email: `%${usernameOrEmail}%`,
            });
        }),
      )
      .getOne();
  }

  findAll() {
    return this.usersRepository.findAndCount({
      select: ['id', 'name', 'username', 'email', 'imageUrl'],
    });
  }

  async findOne(id: string) {
    try {
      return this.usersRepository.findOneOrFail(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'User not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async profile(id) {
    const user = await this.findOne(id);

    delete user['password'];
    delete user['salt'];

    return user;
  }

  async update(id: string, updateUserDto: UpdateUsersDto) {
    try {
      await this.usersRepository.findOneOrFail(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.update(id, {
      ...updateUserDto,
    });

    return this.usersRepository.findOneOrFail(id);
  }

  async remove(id: string) {
    try {
      await this.usersRepository.findOneOrFail(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.delete(id);
  }

  async updatePassword(id, updatePasswordDto) {
    try {
      await this.usersRepository.findOneOrFail(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.update(id, { ...updatePasswordDto });

    return this.usersRepository.findOneOrFail(id);
  }
}
