import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  VersionColumn,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';
import { Order } from '../../order/entities/order.entity';
import { Cart } from '../../cart/entities/cart.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  username: string;

  @Column()
  email: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  imageUrl: string;

  @Column({
    type: 'text',
  })
  password: string;

  @Column()
  salt: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  address: string;

  @OneToMany(
    () => {
      return Order;
    },
    (order) => {
      return order.user;
    },
  )
  orders: Order[];

  @OneToMany(
    () => {
      return Cart;
    },
    (cs) => {
      return cs.user;
    },
  )
  carts: Cart[];

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @VersionColumn()
  version: number;
}
