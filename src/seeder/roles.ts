import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

export default class CreateRoles implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into('roles')
      .values([
        {
          id: '53bce600-0e05-4030-8f42-f4e089f0b67a',
          name: 'user',
          created_at: new Date(),
          updated_at: new Date(),
          deleted_at: null,
          version: 1,
        },
        {
          id: '25a13b9f-7ead-4df1-af32-0de8368f42ea',
          name: 'admin',
          created_at: new Date(),
          updated_at: new Date(),
          deleted_at: null,
          version: 2,
        },
        {
          id: 'd4bf8b53-35fa-45b3-83db-2a5552eee44b',
          name: 'superadmin',
          created_at: new Date(),
          updated_at: new Date(),
          deleted_at: null,
          version: 2,
        },
      ])
      // .orIgnore()
      .orUpdate({
        conflict_target: ['id'],
        overwrite: ['name'],
      })
      .execute();
  }
}
