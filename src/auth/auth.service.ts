import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
} from '@nestjs/common';
import { LoginUserDto } from './dto/login-user.dto';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { hashPassword } from '../helper/hash_password';

@Injectable()
export class AuthService {
  constructor(
    @Inject(
      forwardRef(() => {
        return UsersService;
      }),
    )
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(loginUserDto: LoginUserDto) {
    const user = await this.usersService.findByUsernameOrEmail(
      loginUserDto.usernameOrEmail,
    );

    if (
      user.password !== (await hashPassword(loginUserDto.password, user.salt))
    ) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: 'Incorrect Password',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const payload = {
      email: user.email,
      username: user.username,
      name: user.name,
      id: user.id,
    };

    return {
      access_token: this.jwtService.sign(payload, {
        secret: process.env.SECRET,
      }),
    };
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findByEmail(email);

    if (user && user.password === (await hashPassword(password, user.salt))) {
      const { password, ...result } = user;

      return result;
    }

    return null;
  }
}
