import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { SendMailOptions, Attachment } from 'nodemailer';

@Injectable()
export class EmailService {
  constructor(private readonly mailerService: MailerService) {}

  public send(input: {
    to: string;
    from: string;
    subject: string;
    body: string;
    attachments?: Attachment[];
  }): void {
    const mailerBody: SendMailOptions = {
      to: input.to,
      from: input.from,
      subject: input.subject,
      html: input.body,
      attachments: input.attachments,
    };

    this.mailerService
      .sendMail(mailerBody)
      .then(() => {
        // eslint-disable-next-line no-console
        console.log('Email Success');
      })
      .catch((err) => {
        // eslint-disable-next-line no-console
        console.log(err, 'Email Error');
      });
  }
}
