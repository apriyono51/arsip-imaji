import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Get,
  Query,
  Put,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dto/login-user.dto';
import { Public } from './public.decorator';
import { UsersService } from '../users/users.service';
import { CreateUsersDto } from '../users/dto/create-users.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('v1/auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Public()
  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Login Successful',
      data: await this.authService.login(loginUserDto),
    };
  }

  @Public()
  @Post('register')
  async register(@Body() createUserDto: CreateUsersDto) {
    await this.usersService.create(createUserDto);

    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success create Account',
    };
  }
}
