import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Request,
  UseGuards,
  ParseUUIDPipe,
} from '@nestjs/common';
import { CartService } from './cart.service';
import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import { UsersService } from '../users/users.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@ApiTags('Cart')
@Controller('cart')
export class CartController {
  constructor(
    private readonly cartService: CartService,
    private readonly usersService: UsersService,
  ) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  async create(@Body() createCartDto: CreateCartDto, @Request() req) {
    const user = await this.usersService.findOne(req.user.id);

    return {
      statusCode: HttpStatus.CREATED,
      message: await this.cartService.save(createCartDto, user),
    };
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  async findAllByUser(@Request() req) {
    const user = await this.usersService.findOne(req.user.id);
    const [data, count] = await this.cartService.findAllByUser(user);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.cartService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.cartService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'delete cart success',
    };
  }

  @Delete('all')
  @UseGuards(JwtAuthGuard)
  async removeAll(@Request() req) {
    await this.cartService.removeAll(req.user.id);

    return {
      statusCode: HttpStatus.OK,
      message: 'delete cart success',
    };
  }
}
