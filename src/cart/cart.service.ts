import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateCartDto } from './dto/create-cart.dto';
import { EntityNotFoundError, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from './entities/cart.entity';
import { ProductService } from '../product/product.service';

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    private readonly productService: ProductService,
  ) {}

  async save(createCartDto: CreateCartDto, user) {
    try {
      const product = await this.productService.findOne(createCartDto.product);
      const existCart = await this.findByUserAndProduct(product.id, user.id);

      if (product.stock < createCartDto.quantity) {
        throw new HttpException(
          {
            statusCode: HttpStatus.BAD_REQUEST,
            error: 'stock cannot less than quantity cart',
          },
          HttpStatus.BAD_REQUEST,
        );
      }

      const cartData = {
        quantity: createCartDto.quantity,
        price: product.price,
        totalPrice: product.price * createCartDto.quantity,
      };

      if (existCart) {
        await this.cartRepository.update(existCart.id, cartData);

        return `Success update Product ${product.name}`;
      }

      cartData['user'] = user;
      cartData['product'] = product;

      await this.cartRepository.insert(cartData);

      return 'Success create Product';
    } catch (e) {
      throw e;
    }
  }

  async findAllByUser(user) {
    return this.cartRepository.findAndCount({
      where: { user: user.id },
      relations: ['product'],
    });
  }

  async findOne(id: string) {
    try {
      return await this.cartRepository.findOneOrFail(id, {
        relations: ['product'],
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Cart not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findByUserAndProduct(productId, userId) {
    return this.cartRepository.findOne({
      where: {
        user: userId,
        product: productId,
      },
    });
  }

  async remove(id: string) {
    await this.findOne(id).then(async () => {
      await this.cartRepository.delete(id);
    });
  }

  async removeAll(user: string) {
    const carts = await this.cartRepository.find({
      where: {
        user: user,
      },
    });

    await this.cartRepository.remove(carts);
  }
}
