import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpStatus,
  Put,
  ParseUUIDPipe,
  Query,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Public } from '../auth/public.decorator';
import { ApiTags } from '@nestjs/swagger';
import { GetAllProduct } from './dto/get-all-product.dto';

@ApiTags('Product')
@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post()
  @Public()
  async create(@Body() createProductDto: CreateProductDto) {
    await this.productService.create(createProductDto);

    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success create Product',
    };
  }

  @Get()
  @Public()
  async findAll(@Query() query: GetAllProduct) {
    const [data, count] = await this.productService.findAll(query);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('category')
  @Public()
  async findAllCategory() {
    return {
      data: await this.productService.findAllCategory(),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  @Public()
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.productService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put(':id')
  @Public()
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    return {
      data: await this.productService.update(id, updateProductDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id')
  @Public()
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.productService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
