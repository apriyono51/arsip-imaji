import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { EntityNotFoundError, Repository } from 'typeorm';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    return this.productRepository.insert({ ...createProductDto });
  }

  async findAll(request) {
    let query;

    if (request.category) {
      query = await this.productRepository.findAndCount({
        where: { category: request.category },
      });
    } else {
      query = await this.productRepository.findAndCount();
    }

    return query;
  }

  async findAllCategory() {
    const categories = await this.productRepository
      .createQueryBuilder('product')
      .select('DISTINCT product.category', 'name')
      .getRawMany();

    return categories.map((c) => {
      return c.name;
    });
  }

  async findOne(id: string) {
    try {
      return await this.productRepository.findOneOrFail(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Product not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updateProductDto: UpdateProductDto) {
    try {
      await this.productRepository.findOneOrFail(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.productRepository.update(id, { ...updateProductDto });

    return this.productRepository.findOneOrFail(id);
  }

  async remove(id: string) {
    await this.findOne(id).then(async () => {
      await this.productRepository.delete(id);
    });
  }
}
