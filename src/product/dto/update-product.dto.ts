import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateProductDto {
  @ApiProperty()
  @IsOptional()
    name: string;

  @ApiProperty()
  @IsOptional()
    imageUrl: string;

  @ApiProperty()
  @IsOptional()
    category: string;

  @ApiProperty()
  @IsOptional()
    price: number;

  @ApiProperty()
  @IsOptional()
    stock: number;
}
