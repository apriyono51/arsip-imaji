import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class GetAllProduct {
  @ApiPropertyOptional()
  @IsOptional()
  category: string;
}
