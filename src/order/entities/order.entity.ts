import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Product } from '../../product/entities/product.entity';
import { Users } from '../../users/entities/users.entity';

@Entity()
export class Order extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(
    () => {
      return Product;
    },
    (Product) => {
      return Product.id;
    },
  )
  product: Product;

  @ManyToOne(
    () => {
      return Users;
    },
    (Users) => {
      return Users.id;
    },
  )
  user: Users;

  @Column({
    default: 0,
  })
  quantity: number;

  @Column({
    default: 0,
  })
  totalPrice: number;

  @Column({
    nullable: false,
  })
  status: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @VersionColumn()
  version: number;
}
