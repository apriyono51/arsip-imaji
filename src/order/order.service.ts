import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Cart } from '../cart/entities/cart.entity';
import { CreateOrderProductDto } from './dto/create-order-product.dto';
import { ProductService } from '../product/product.service';
import * as _ from 'lodash';
import { Product } from '../product/entities/product.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    private readonly productService: ProductService,
  ) {}

  async create(createOrderDto: CreateOrderDto, user) {
    createOrderDto.cartIds.map(async (cartId) => {
      const cart = await this.cartRepository.findOne(cartId, {
        select: ['product', 'user', 'quantity', 'totalPrice'],
        relations: ['product'],
      });

      const product = await this.productService.findOne(cart.product.id);

      const order = {
        ...cart,
        user,
        product,
        status: 'waiting',
      };

      await this.orderRepository.insert(order).then(async () => {
        await this.cartRepository.delete(cartId);
      });
    });

    return 'order Created with items on cart';
  }

  async createOne(createOrderProductDto: CreateOrderProductDto, user) {
    const product = await this.productService.findOne(
      createOrderProductDto.product,
    );

    if (product.stock < createOrderProductDto.quantity) {
      throw new HttpException(
        {
          statusCode: HttpStatus.BAD_REQUEST,
          error: 'stock cannot less than quantity cart',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const order = {
      user: user,
      product: product,
      quantity: createOrderProductDto.quantity,
      totalPrice: product.price * createOrderProductDto.quantity,
      status: 'waiting',
    };

    await this.orderRepository.insert(order);

    return 'order Created';
  }

  async findAll(user) {
    return this.orderRepository.findAndCount({
      where: {
        user: user.id,
      },
      relations: ['product'],
    });
  }

  async findOne(id: string) {
    try {
      return await this.orderRepository.findOneOrFail(id, {
        relations: ['product'],
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Order not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(updateOrderDto: UpdateOrderDto, user) {
    const orders = await this.orderRepository.find({
      relations: ['product'],
      where: {
        user: user.id,
        status: 'waiting',
      },
    });

    if (updateOrderDto.status === 'success') {
      await Promise.all(
        orders.map(async (order) => {
          const product = await this.productService.findOne(order.product.id);

          await this.productRepository.update(order.product.id, {
            stock: product.stock - order.quantity,
          });
        }),
      );

      await setTimeout(async () => {
        await this.orderRepository
          .createQueryBuilder()
          .update('order')
          .set({ status: 'completed' })
          .where('user = :id', { id: user.id })
          .andWhere('status = :status', { status: 'success' })
          .execute();
      }, 3000);
    }

    await this.orderRepository
      .createQueryBuilder()
      .update('order')
      .set({ status: updateOrderDto.status })
      .where('user = :id', { id: user.id })
      .andWhere('status = :status', { status: 'waiting' })
      .execute();

    return this.orderRepository.find({ where: { user: user.id } });
  }

  async remove(id: string) {
    await this.findOne(id).then(async () => {
      await this.cartRepository.delete(id);
    });
  }
}
