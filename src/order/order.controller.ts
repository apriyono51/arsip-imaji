import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  UseGuards,
  Request,
  ParseUUIDPipe,
  Put,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UsersService } from '../users/users.service';
import { CreateOrderProductDto } from './dto/create-order-product.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Order')
@UseGuards(JwtAuthGuard)
@Controller('order')
export class OrderController {
  constructor(
    private readonly orderService: OrderService,

    private readonly usersService: UsersService,
  ) {}

  @Post('cart')
  async createCart(@Body() createOrderDto: CreateOrderDto, @Request() req) {
    const user = await this.usersService.findOne(req.user.id);

    return {
      statusCode: HttpStatus.CREATED,
      message: await this.orderService.create(createOrderDto, user),
    };
  }

  @Post('product')
  async createOne(
    @Body() createOrderProductDto: CreateOrderProductDto,
    @Request() req,
  ) {
    const user = await this.usersService.findOne(req.user.id);

    return {
      statusCode: HttpStatus.CREATED,
      message: await this.orderService.createOne(createOrderProductDto, user),
    };
  }

  @Get()
  async findAll(@Request() req) {
    const user = await this.usersService.findOne(req.user.id);
    const [data, count] = await this.orderService.findAll(user);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return {
      data: await this.orderService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('status')
  async update(@Body() updateOrderDto: UpdateOrderDto, @Request() req) {
    const user = await this.usersService.findOne(req.user.id);

    return {
      data: await this.orderService.update(updateOrderDto, user),
      statusCode: HttpStatus.OK,
      message: 'delete order success',
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.orderService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'delete order success',
    };
  }
}
